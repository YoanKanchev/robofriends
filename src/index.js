import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './containers/App';
import {applyMiddleware, combineReducers, createStore} from "redux";
import 'tachyons';
import {createLogger} from "redux-logger";
import thunkMiddleware from "redux-thunk";
import {Provider} from 'react-redux';
import {requestRobots, searchRobots} from "./redux/reducers";

const logger = createLogger();
const rootReducer = combineReducers({requestRobots, searchRobots});
const store = createStore(rootReducer, applyMiddleware(thunkMiddleware, logger));


ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>, document.getElementById('root'));

